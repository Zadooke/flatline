﻿Shader "Unlit/Water"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Noise ("Water Noise", 2D) = "white" {}
		_Outline ("Outline", 2D) = "white" {}
		_Lines ("High Lines", 2D) = "white" {}

		_Color1 ("Water Light", Color) = (1, 1, 1, 1)
		_Color2 ("Water Dark", Color) = (1, 1, 1, 1)
		_BorderColor("Border Color", Color) = (1, 1, 1, 1)
		_HightColor ("High Color", Color) = (1, 1, 1, 1)

		_Frequency ("Frequency", Range(1, 10)) = 8.0
		_Magnitude ("Magnitude", Range(0, 1)) = 0.1
		_Depth ("Depth", Float) = 1
	}
	SubShader
	{
		Tags
		{
			"Queue" = "Transparent"
			"RenderType"="Transparent"
			"PreviewType" = "Plane"
		}
		LOD 100

		Pass
		{
			ZWrite On
			Cull Off
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma glsl
			#pragma target 3.0
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float4 worldPos : TEXCOORD1;
				float4 vertexPos : TEXCOORD2;
			};


			sampler2D _MainTex, _Noise, _Outline, _Lines;

			float4 _MainTex_ST;
			float4 _Color1, _Color2, _BorderColor, _HightColor;
			
			float _Frequency, _Magnitude, _Depth;

			fixed _Height;

			v2f vert (appdata v)
			{
				v2f o;
				
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.worldPos = mul(unity_ObjectToWorld, v.vertex);

				v.vertex.x += sin((_Time.y + o.worldPos.y) + _Frequency * v.uv.y) * _Magnitude;
				v.vertex.y += cos((_Time.y + o.worldPos.x) + _Frequency * v.uv.x) * _Magnitude;
				//v.vertex.z += cos((_Time.y + o.worldPos.x) + _Frequency * v.uv.x) * _Magnitude;						

				o.vertexPos.x = v.vertex.z;

				o.vertex = UnityObjectToClipPos(v.vertex);

				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv);
				fixed4 noise = tex2D(_Noise, i.uv + _Time.x);
				fixed4 outline = tex2D(_Outline, i.uv);
				fixed4 high = tex2D(_Lines, i.uv);

				fixed fresnel = cos((_Time.y + i.worldPos.y) + _Frequency * i.uv.x) * _Magnitude;

				high *= ((sin(_Time.y) + 1) / 2) + 0.5;
				high *= outline;

				// Cor Base
				col.rgb = _Color1.rgb;
				
				// Textura
				col.rgb += (_BorderColor * (noise - outline)) * 0.2 * ((fresnel + 1) / 2);
				
				// Borda
				col.rgb += _BorderColor * outline;
				
				// Profundidade
				col.rgb -= float3(i.vertexPos.x, i.vertexPos.x, i.vertexPos.x);

				// High
				col.rgb += high;

				// Alpha
				col.a = outline + 0.2;
				
				return col;
			}
			ENDCG
		}
	}
}
