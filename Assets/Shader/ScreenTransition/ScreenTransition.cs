﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ScreenTransition : MonoBehaviour
{
    public Material mat;
    public float Lerp = 0.0f;

    public float Speed = 5.0f;

    private IEnumerator CBlack()
    {
        Lerp = 0.0f;

        while(Lerp < 1.0f)
        {
            Lerp += Time.unscaledDeltaTime  * Speed;
            mat.SetFloat("_Lerp", Lerp);

            yield return null;
        }

        Lerp = 1.0f;
        mat.SetFloat("_Lerp", Lerp);

        yield return null;
    }
    private IEnumerator CWhite()
    {
        Lerp = 1.0f;

        while (Lerp > 0.0f)
        {
            Lerp -= Time.unscaledDeltaTime * Speed;
            mat.SetFloat("_Lerp", Lerp);

            yield return null;
        }

        Lerp = 0.0f;
        mat.SetFloat("_Lerp", Lerp);

        yield return null;
    }

    public void Black()
    {
        StartCoroutine(CBlack());
    }

    public void White()
    {
        StartCoroutine(CWhite());
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (mat == null)
            return;

        Graphics.Blit(source, destination, mat);
    }

    private void OnDisable()
    {
        mat.SetFloat("_Lerp", 0.0f);
    }

    private void OnDestroy()
    {
        mat.SetFloat("_Lerp", 0.0f);
    }

    void Start()
    {
        //Player.Instance.Died.AddListener(Black);
    }

    void Update()
    {

    }
}
