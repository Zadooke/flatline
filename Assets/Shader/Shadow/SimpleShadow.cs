﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleShadow : MonoBehaviour
{
    private SpriteRenderer _renderer;
    private SpriteRenderer _shadow;

    public Color ShadowColor;

    public Vector2 PositionOffset;
    public Vector2 ScaleOffset;

    private void Awake()
    {
        _renderer = GetComponent<SpriteRenderer>();
    }

    void Start()
    {
        GameObject go = new GameObject(gameObject.name + " Shadow");

        go.transform.parent = transform;
        go.transform.localPosition = PositionOffset;
        go.transform.localScale = ScaleOffset;

        _shadow = go.AddComponent<SpriteRenderer>();

        _shadow.sprite = _renderer.sprite;
        _shadow.color = ShadowColor;

        _shadow.sortingLayerID = _renderer.sortingLayerID;
        _shadow.sortingOrder = -100;
    }

    void Update()
    {
        _shadow.sprite = _renderer.sprite;
        _shadow.flipX = _renderer.flipX;
    }
}
