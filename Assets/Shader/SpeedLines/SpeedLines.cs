﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpeedLines : MonoBehaviour
{
    private Image _image;
    private PlayerController _player;

    private float _maxOpacity = 0.2f;

    private void Awake()
    {
        _image = GetComponent<Image>();
    }

    void Start()
    {
        _player = Player.Instance.GetComponent<PlayerController>();
    }

    void Update()
    {
        var vel = Mathf.Abs(_player.Velocity.x);

        if (vel >= 75)
            _image.color = new Color(_image.color.r, _image.color.b, _image.color.g, _maxOpacity);
        else
            _image.color = new Color(_image.color.r, _image.color.b, _image.color.g, 0.0f);
    }
}
