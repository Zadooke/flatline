﻿Shader "Unlit/Sky"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Height("Height", Float) = 1
		
		_Hue("Hue", Range(1, 360)) = 1
		_Exposure("Exposure", Range(0.0, 0.025)) = 0

		_Offset("Offset", Float) = 0

		_TopColor ("Top Color", Color) = (1, 1, 1, 1)
     	_BottomColor ("Bottom Color", Color) = (1, 1, 1, 1)
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" "Queue"="Transparent"}
		LOD 100

		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
		
			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			
			fixed4 _TopColor, _BottomColor;
			fixed _Height, _Hue, _Exposure, _Offset;

			inline float3 applyHue(float3 aColor, float aHue)
			{
				float angle = radians(aHue);
				float3 k = float3(0.57735, 0.57735, 0.57735);
				float cosAngle = cos(angle);

				//Rodrigues' rotation formula
				return aColor * cosAngle + cross(k, aColor) * sin(angle) + k * dot(k, aColor) * (1 - cosAngle);
			}

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				_Hue += sin(_Time.x) * 360;
				_BottomColor.rgb = applyHue(_BottomColor.rgb, _Hue);

				// Aplica Exposure apenas se nas bordas
				float x = (i.uv.x * 2) - 1;	// -1 -> 1 eixo X
				x = abs(x);

				_TopColor += _Exposure * x;
				_BottomColor += _Exposure * x;

				return lerp(_BottomColor, _TopColor, i.uv.y * _Height);
			}
			ENDCG
		}
	}
}
