﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InGameCanvasManager : MonoBehaviour
{
    public GameObject MainCanvas;
    public GameObject MenuCanvas;
    public GameObject EndLevelCanvas;

    [Header("Main Canvas")]
    public Text Speed;
    public Image Velocimeter;

    public Gradient VelocimeterColors;

    private PlayerController _player;

    [Header("InGame Menu")]
    public Text LevelName_Menu;
    public Text LevelTime_Menu;

    public bool OnMenu = false;

    public Button ButtonContinue;

    [Header("End Level")]
    public Text LevelTime_End;
    public Text BestTime_End;
    public Text Split;

    public Button ButtonNext;

    private string _bestTime = "and this your is Personal Best!";
    private string _oldBest = "and your best time was ";

    private static InGameCanvasManager _instance;
    public static InGameCanvasManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<InGameCanvasManager>();

            return _instance;
        }
    }

    public void OpenMenu()
    {
        Player.Instance.Active = false;

        Time.timeScale = 0.0f;
        OnMenu = true;

        MenuCanvas.SetActive(true);
        MainCanvas.SetActive(false);

        MenuCanvas.GetComponent<Animator>().SetTrigger("Enter");

        ButtonContinue.Select();

        LevelName_Menu.text = Level.Instance.LevelName;
        LevelTime_Menu.text = Timer.Instance.Text.text;
    }

    public void CloseMenu()
    {
        Player.Instance.Active = true;

        Time.timeScale = 1.0f;

        MenuCanvas.GetComponent<Animator>().SetTrigger("Exit");

        EventSystem.current.SetSelectedGameObject(null);

        OnMenu = false;

        //MenuCanvas.SetActive(false);
        MainCanvas.SetActive(true);
    }

    public void NextLevel()
    {
        Camera.main.GetComponent<ScreenTransition>().Black();

        Level.Instance.LoadNextLevel();
    }

    /// <summary>
    /// Abre o Panel de fim de level
    /// </summary>
    public void EndLevel()
    {
        MenuCanvas.SetActive(false);
        MainCanvas.SetActive(false);
        EndLevelCanvas.SetActive(true);

        ButtonNext.Select();

        LevelTime_End.text = Timer.Instance.Text.text;

        // Checa se esse é o melhor tempo
        var currentBest = Level.Instance.GetBestTime();

        if (currentBest > Timer.Instance.CurrentTime)
            BestTime_End.text = _bestTime;
        else
            BestTime_End.text = _oldBest + Timer.FloatToTime(currentBest);

        Split.color = Timer.Instance.Bronze;
        Split.text = "Bronze Split...";

        if (Timer.Instance.CurrentSplit == Timer.Split.Silver)
        {
            Split.text = "Silver Split";
            Split.color = Timer.Instance.Silver;
        }

        if (Timer.Instance.CurrentSplit == Timer.Split.Gold)
        {
            Split.text = "Gold Split!";
            Split.color = Timer.Instance.Gold;
        }

        Level.Instance.UpdateTime(Timer.Instance.CurrentTime);
        Level.Instance.Unlock(Level.Instance.ID + 1);
    }

    public void Restart()
    {
        Level.Instance.Restart();
    }

    public void GoToMainMenu()
    {
        Time.timeScale = 1.0f;

        Level.Instance.Menu();
    }


    void Start()
    {
        MenuCanvas.SetActive(false);
        MainCanvas.SetActive(true);
        EndLevelCanvas.SetActive(false);

        _player = Player.Instance.GetComponent<PlayerController>();
    }

    void Update()
    {
        var amount = Mathf.Abs(_player.Velocity.x) / 75;

        Velocimeter.fillAmount = amount;
        Velocimeter.color = VelocimeterColors.Evaluate(amount);

        Speed.text = ((int)Mathf.Abs(_player.Velocity.x)).ToString();// + "x 10^6 m/s";
    }
}
