﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{
    private Player _player;
    private End _end;

    public Image Bar;

    private float _initialX;
    private float _endX;

    void Start()
    {
        _end = FindObjectOfType<End>();
        _player = Player.Instance;

        if (_end == null)
        {
            gameObject.SetActive(false);
            return;
        }

        _initialX = _player.transform.position.x;
        _endX = _end.transform.position.x;
    }

    void Update()
    {
        if (Bar == null || _player == null)
            return;

        // Atualiza a posição atual do jogador na barra
        /*
            init    = 0
            end     = 1
            current = x
            
            x = current / end         
         */

        var current = _player.transform.position.x;
        var x = current / _endX;

        Bar.fillAmount = x;
    }
}
