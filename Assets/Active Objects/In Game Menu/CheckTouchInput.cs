﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Desabilita o GameObject se não for Touch
/// </summary>
public class CheckTouchInput : MonoBehaviour
{
    public bool EnableWhenTouch = true;

    void Start()
    {
        gameObject.SetActive(false);

        var isTouch = (Application.platform == RuntimePlatform.Android);

        if(isTouch && EnableWhenTouch)
            gameObject.SetActive(true);

        if(!isTouch && !EnableWhenTouch)
            gameObject.SetActive(true);
    }
}
