﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : PlatformerBody
{
    private Player _player;
    private SpriteRenderer _renderer;

    private Follow _camera;

    public ParticleSystem Trail;
    public Animator Animator;

    public Cable CurrentCable = null;

    [HideInInspector] public bool _holdingJump = false;
    [HideInInspector] public bool PressedDown = false;

    private bool _goingDown = false;
    private bool _quickDrop = false;

    public float CurrentMaxVelocity;

    private float _velocityChange;
    private float _velocityChangeSpeed;

    public bool CanJump = true;
    public bool CanDrop = true;

    public enum ControllerType
    {
        Keyboard,
        Swipe,
        OnScreen
    }
    public ControllerType Type;

    [Header("Audio")]
    public AudioClip Jump;
    public AudioClip Explode;
    public AudioClip Land;
    public AudioClip Drop;

    private AudioSource _audioSource;

    [Header("TouchControls")]
    public float TouchDragLimiter = 0.0f;

    public float InitialTouchPos;
    public float CurrentTouchPos;

    [Header("Checkpoints")]
    public GameObject Checkpoint;
    private GameObject _checkpoint;

    [Header("Water")]
    public bool OnWater = false;
    public float WaterGravity = 0.1f;

    public bool Riding = false;

    #region Water

    public void EnterWater()
    {
        OnWater = true;
    }

    public void ExitWater()
    {
        OnWater = false;
    }

    #endregion

    #region Input Handlers

    private void HandleTouchJump(int id)
    {
        if (Input.GetMouseButtonDown(id))
            InitialTouchPos = Input.mousePosition.y;

        if (Input.GetMouseButtonUp(id))
        {
            InitialTouchPos = CurrentTouchPos;
            _holdingJump = false;
        }

        if (Input.GetMouseButton(id))
        {
            CurrentTouchPos = Input.mousePosition.y;

            // Ve pulo
            var delta = CurrentTouchPos - InitialTouchPos;

            if (Mathf.Abs(delta) > TouchDragLimiter)
            {
                if (delta >= 0)
                {
                    if (!CanJump)
                        return;

                    if (Grounded || OnWater || Riding)
                    {
                        // Vibra
                        //Handheld.Vibrate();

                        // Pula
                        PressedJump = true;
                        //_audioSource.PlayOneShot(Jump);
                    }

                    _holdingJump = true;
                    InitialTouchPos = CurrentTouchPos;
                }
                else
                {
                    if (!CanDrop)
                        return;

                    PressedDown = true;
                    InitialTouchPos = CurrentTouchPos;
                }
            }
        }
    }

    public void HandleTouchInput()
    {
        LastXInput = xInput;

        xInput = 0;

        if (Input.touchCount <= 0)
            return;

        var first = Input.GetTouch(0);

        var camera = Camera.main;
        var firstPos = camera.ScreenToViewportPoint(first.position.xyz(0));

        bool horizontal = false;
        bool vertical = false;

        if (firstPos.x < 0.5f) // Primeiro toque está pra esquerda
        {
            // Ve direção de movimento
            xInput = (firstPos.x >= 0.20f) ? xInput = 1 : xInput = -1;

            horizontal = true;
        }
        else
        {
            HandleTouchJump(0);

            vertical = true;
        }

        if (Input.touchCount <= 1)
            return;

        var second = Input.GetTouch(1);
        var secondPos = camera.WorldToViewportPoint(second.position.xyz(0));

        if (secondPos.x < 0.5f && !horizontal) // Primeiro toque está pra esquerda
        {
            // Ve direção de movimento
            xInput = (secondPos.x >= 0.20f) ? xInput = 1 : xInput = -1;
        }
        else
        {
            if (vertical)
                return;

            HandleTouchJump(1);
        }

    }

    private void HandleInput()
    {
        xInput = Input.GetAxisRaw("Horizontal");

        if (CanJump)
        {
            if (Input.GetButtonDown("Jump") && (Grounded || OnWater || Riding))
            {
                //_audioSource.PlayOneShot(Jump);
                PressedJump = true;
            }
        }

        if (CanDrop)
        {
            if (Input.GetButtonDown("Down"))
            {
                PressedDown = true;
            }
        }

        _holdingJump = Input.GetButton("Jump");

        if (Input.GetButtonDown("Reset"))
            Level.Instance.Restart();
    }


    #endregion

    #region Physics Handlers

    public void AddJumpVelocity(float vel = 1)
    {
        Velocity.y = JumpVelocity * vel;

        _jumping = true;

        Grounded = false;
        _checkGround = false;

        _audioSource.PlayOneShot(Jump);
    }

    protected override bool CheckGround(Vector3 position)
    {
        if (_goingDown)
            return false;

        var offset = Vector2.left * (_col.bounds.extents.x - 0.1f);
        float dist = 0.5f;

        Debug.DrawRay(position.xy() + offset, Vector2.down * CheckGroundDistance, Color.red);
        Debug.DrawRay(position.xy() - offset, Vector2.down * CheckGroundDistance, Color.red);

        var groundLeft = Physics2D.Raycast(position.xy() + offset, Vector2.down, dist, _groundLayer);
        var groundRight = Physics2D.Raycast(position.xy() - offset, Vector2.down, dist, _groundLayer);

        if (groundLeft && _col.IsTouchingLayers(_groundLayer))
        {
            return UpdateCable(groundLeft);
        }
        else if (groundRight && _col.IsTouchingLayers(_groundLayer))
        {
            return UpdateCable(groundRight);
        }

        return false;
    }

    protected override void HandleJump()
    {
        if (PressedDown && _jumping && !Grounded)
        {
            _audioSource.PlayOneShot(Drop);

            PressedDown = false;
            _quickDrop = true;
            Velocity.y = -JumpVelocity;
        }

        if (CanDrop)
        {
            if (PressedDown && Grounded)
            {
                Animator.SetTrigger("Jump");
                _audioSource.PlayOneShot(Drop);

                _jumping = true;
                Grounded = false;

                _goingDown = true;

                _checkGround = false;
                _col.isTrigger = true;
            }
        }

        if (_goingDown && !_col.IsTouchingLayers(_groundLayer))
        {
            _goingDown = false;
            _col.isTrigger = false;
        }

        if (CanJump)
        {
            if (PressedJump && (Grounded || OnWater))
            {
                _audioSource.PlayOneShot(Jump);
                Animator.ResetTrigger("Land");
                Animator.SetTrigger("Jump");

                PressedJump = false;

                if (OnWater)
                    _quickDrop = false;

                Velocity.y = JumpVelocity;

                _jumping = true;

                Grounded = false;
                _checkGround = false;

                //CurrentCable.OnExit.Invoke();
            }

            // Ainda está pulando, e não está apertando o botão de pulo
            if (!_holdingJump && !Grounded && Velocity.y > 0)
            {
                Velocity.y += _highGravity * Time.fixedDeltaTime;
            }
        }

        // Checa se precisa checar ground
        if (Velocity.y < 0 && UseGravity)
        {
            _checkGround = true;

            if (_jumping)
                Animator.SetTrigger("Fall");
        }

        // Faz a checagem
        if (_checkGround)
        {
            if (CheckGround(transform.position + Vector3.down * (_col.bounds.extents.y + 0.1f)))
            {
                if (_jumping)
                {
                    Animator.ResetTrigger("Fall");
                    Animator.SetTrigger("Land");

                    if (_quickDrop)
                    {
                        _quickDrop = false;
                        _camera.Shake();
                    }

                    _audioSource.PlayOneShot(Land);
                }

                Grounded = true;
                _jumping = false;

                Velocity.y = 0;
                _checkGround = false;
            }
            else
            {
                Grounded = false;
                _checkGround = true;
            }
        }

    }

    protected override void HandleHorizontalMovement()
    {
        if (CurrentCable != null)
        {
            // Checa se esta diminuindo ou aumentando a velocidade, se estiver aumentando, muda automaticamente
            if (CurrentCable.MaxVelocity > CurrentMaxVelocity)
                CurrentMaxVelocity = CurrentCable.MaxVelocity;

            CurrentMaxVelocity = Mathf.SmoothDamp(CurrentMaxVelocity, CurrentCable.MaxVelocity, ref _velocityChange, 0.25f);
        }

        if ((xInput == 0 || xInput != LastXInput) && Grounded)
        {
            float decreaseSpeed = -Velocity.x * FloorFriction * 20;
            Velocity.x = Mathf.SmoothDamp(Velocity.x, 0, ref decreaseSpeed, 1.0f, MaxHorizontalSpeed, Time.fixedDeltaTime);
        }

        if (Grounded)
            LastXInput = xInput;

        // Adiciona Momentum
        if (Grounded)
        {
            if (xInput == 0)
                Animator.SetBool("Moving", false);
            else
            {
                _renderer.flipX = (xInput < 0);
                Animator.SetBool("Moving", true);
            }

            HorizontalMomentum = xInput * GroundAcceleration;

            var newSpeed = Velocity.x + HorizontalMomentum * Time.fixedDeltaTime;

            if (newSpeed > 0)
            {
                if (Velocity.x > CurrentMaxVelocity)
                    Velocity.x = Mathf.SmoothDamp(Velocity.x, CurrentMaxVelocity, ref _velocityChangeSpeed, 0.1f);
                else
                    Velocity.x = (newSpeed < MaxHorizontalSpeed) ? newSpeed : CurrentMaxVelocity;
            }
            else
            {
                if (Velocity.x < -CurrentMaxVelocity)
                    Velocity.x = Mathf.SmoothDamp(Velocity.x, -CurrentMaxVelocity, ref _velocityChangeSpeed, 0.1f);
                else
                    Velocity.x = (newSpeed > -MaxHorizontalSpeed) ? newSpeed : -CurrentMaxVelocity;
            }
        }
        else if (!Grounded && OnWater)
        {
            if (xInput == 0)
                Animator.SetBool("Moving", false);
            else
            {
                _renderer.flipX = (xInput < 0);
                Animator.SetBool("Moving", true);
            }

            HorizontalMomentum = xInput * GroundAcceleration;

            var newSpeed = Velocity.x + HorizontalMomentum * Time.fixedDeltaTime;

            if (newSpeed > 0)
            {
                if (Velocity.x > MaxHorizontalSpeed)
                    Velocity.x = Mathf.SmoothDamp(Velocity.x, MaxHorizontalSpeed, ref _velocityChangeSpeed, 0.5f);
                else
                    Velocity.x = (newSpeed < MaxHorizontalSpeed) ? newSpeed : MaxHorizontalSpeed;
            }
            else
            {
                if (Velocity.x < -MaxHorizontalSpeed)
                    Velocity.x = Mathf.SmoothDamp(Velocity.x, -MaxHorizontalSpeed, ref _velocityChangeSpeed, 0.15f);
                else
                    Velocity.x = (newSpeed > -MaxHorizontalSpeed) ? newSpeed : -MaxHorizontalSpeed;
            }
        }
        else
        {
            HorizontalMomentum = xInput * AirAcceleration;
            var newSpeed = Velocity.x + HorizontalMomentum * Time.fixedDeltaTime;

            if (xInput > 0) // Direita
            {
                Velocity.x = Mathf.Min(newSpeed, MaxHorizontalSpeed);
            }
            else if (xInput < 0) // Esquerda
            {
                Velocity.x = Mathf.Max(newSpeed, -MaxHorizontalSpeed);
            }

            if (xInput == 0 || xInput != LastXInput)
            {
                if (Velocity.x > MaxHorizontalSpeed || Velocity.x < -MaxHorizontalSpeed)
                {
                    float decreaseSpeed = -Velocity.x * FloorFriction * 10;
                    Velocity.x = Mathf.SmoothDamp(Velocity.x, 0, ref decreaseSpeed, 1.0f, MaxHorizontalSpeed * 2, Time.fixedDeltaTime);

                }
            }
        }
    }

    protected void HandleGravity()
    {
        if (OnWater)
        {
            if (_jumping)
            {
                if (Velocity.y > 0)
                    Velocity.y = Mathf.Max(Velocity.y + Gravity * WaterGravity * Time.fixedDeltaTime, -MaxFallSpeed);
                else
                    Velocity.y = Mathf.Max(Velocity.y + _highGravity * WaterGravity * Time.fixedDeltaTime, -MaxFallSpeed);
            }
            else
            {
                var accel = _highGravity * WaterGravity * Time.fixedDeltaTime;

                if (Velocity.y + accel < -MaxFallSpeed)
                    Velocity.y = -MaxFallSpeed;
                else
                    Velocity.y += accel;
            }
        }
        else
        {
            if (_jumping)
            {
                if (Velocity.y > 0)
                    Velocity.y = Mathf.Max(Velocity.y + Gravity * Time.fixedDeltaTime, -MaxFallSpeed);
                else
                    Velocity.y = Mathf.Max(Velocity.y + _highGravity * Time.fixedDeltaTime, -MaxFallSpeed);
            }
            else
            {
                var accel = _highGravity * Time.fixedDeltaTime;

                if (Velocity.y + accel < -MaxFallSpeed)
                    Velocity.y = -MaxFallSpeed;
                else
                    Velocity.y += accel;
            }
        }
    }

    #endregion

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.GetComponent<Cable>() == CurrentCable)
        {
            _col.isTrigger = false;
            _goingDown = false;
        }
    }

    public void PlaceCheckpoint(Vector3 pos)
    {
        // Checa se não está na mesma posição
        if (_player.CheckpointPosition == pos)
            return;

        // Toca um Som

        // Instancia um Objeto
        if (_checkpoint == null)
            _checkpoint = Instantiate(Checkpoint);

        _checkpoint.transform.position = pos;

        // Marca posição no Player
        _player.CheckpointPosition = pos; // + Vector3.up * 0.5f;

        // Marca o Tempo
        _player.CheckpointTime = Timer.Instance.CurrentTime;
    }

    private bool UpdateCable(RaycastHit2D hit)
    {
        if (hit.collider.GetComponent<Transformer>())
        {
            // Double Jump
            Animator.SetTrigger("Jump");

            return false;
        }
        else
        {
            // Atualiza Cable
            CurrentCable = hit.collider.GetComponent<Cable>();

            CurrentCable.OnEnter.Invoke();

            _renderer.color = CurrentCable.GetComponent<SpriteRenderer>().color;

            var trailMain = Trail.main;
            trailMain.startColor = _renderer.color;

            Animator.ResetTrigger("Jump");

            return true;
        }
    }

    protected override void Move()
    {
        /* ---------- Move ---------- */
        if (CheckHorizontalMovement)
            HandleHorizontalMovement();

        /* ---------- Pula ---------- */
        if (CheckJump)
            HandleJump();

        /*-------- Gravidade --------*/
        if (UseGravity)
        {
            HandleGravity();
        }

        // Retorna os valores para a Engine de fisica da Unity
        var nextPosition = transform.position.xy() + Velocity * Time.fixedDeltaTime;
        _body.MovePosition(nextPosition);
    }

    public void Die()
    {
        _col.enabled = false;

        _camera.Shake(1.5f, 1.0f);

        _audioSource.PlayOneShot(Explode);
        Grounded = false;
        _quickDrop = false;
        _goingDown = false;
    }

    private void Awake()
    {
        base.Awake();

        Animator = GetComponent<Animator>();

        _audioSource = GetComponent<AudioSource>();

        _player = GetComponent<Player>();
        _renderer = GetComponent<SpriteRenderer>();
        _camera = Camera.main.GetComponent<Follow>();
    }

    void Start()
    {
        base.Start();

        Input.multiTouchEnabled = true;

        // Checa se está em um PC ou em Mobile
        if (Application.platform == RuntimePlatform.Android)
        {
            if (PlayerSettingsSave.Load().SwipeControlls)
                Type = ControllerType.Swipe;
            else
                Type = ControllerType.OnScreen;
        }
        else
        {
            Type = ControllerType.Keyboard;
        }
    }

    void Update()
    {
        base.Update();

        if (_player.Active)
        {
            switch (Type)
            {
                case ControllerType.Keyboard:
                    HandleInput();
                    break;
                case ControllerType.Swipe:
                    HandleTouchInput();
                    break;
                case ControllerType.OnScreen:
                    break;
            }

            if (transform.position.y < -10)
                _player.Die();
        }

    }

    private void FixedUpdate()
    {
        if (_player.Active)
            base.FixedUpdate();
    }
}
