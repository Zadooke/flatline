﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    public bool Active = false;
    public bool HasStarted = false;

    public GameObject DeadParticles;

    public UnityEvent Died;
    public UnityEvent StartedEvent;

    public Vector3 CheckpointPosition;
    public float CheckpointTime;

    private static Player _instance;
    public static Player Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<Player>();

            return _instance;
        }
    }

    public PlayerController Controller
    {
        get
        {
            return GetComponent<PlayerController>();
        }
    }

    public bool PressedAnithing()
    {
        return Input.anyKeyDown;
    }

    public void Die()
    {
        GetComponent<PlayerController>().Die();

        // Recarrega a própria cena
        Active = false;
        StartCoroutine(CReload());
    }

    /// <summary>
    /// Retorna o Player para o Ultimo Checkpoint
    /// </summary>
    public void Restart()
    {
        transform.position = CheckpointPosition;

        if(Timer.Instance != null)
        {
            Timer.Instance.CurrentTime = CheckpointTime;
            Timer.Instance.SetText(CheckpointTime);
            Timer.Instance.Counting = false;
        }


        Camera.main.GetComponent<ScreenTransition>().White();
        GetComponent<PlayerController>().enabled = true;
        GetComponent<SpriteRenderer>().enabled = true;
        GetComponent<Collider2D>().enabled = true;

        Active = true;
        HasStarted = false;
    }

    public void Explode()
    {
        Destroy(this.gameObject);
        Instantiate(DeadParticles, transform.position, Quaternion.identity);
    }

    private IEnumerator CReload()
    {
        GetComponent<PlayerController>().Velocity = Vector2.zero;
        GetComponent<PlayerController>().enabled = false;
        GetComponent<SpriteRenderer>().enabled = false;

        var scene = SceneManager.GetActiveScene();

        Time.timeScale = 0.3f;

        Instantiate(DeadParticles, transform.position, Quaternion.identity);

        Camera.main.GetComponent<Follow>().Shake(0.5f, 1.0f);

        //System.Func<bool> pressed = new Func<bool>(PressedAnithing);

        //yield return new WaitUntil(pressed);

        Died.Invoke();

        yield return new WaitForSecondsRealtime(1.0f);

        Camera.main.GetComponent<ScreenTransition>().Black();
        Time.timeScale = 1.0f;

        yield return new WaitForSecondsRealtime(0.5f);

        Restart();

        //SceneManager.LoadScene(scene.name);

        yield return null;
    }

    void Start()
    {
        CheckpointPosition = transform.position;
        Camera.main.GetComponent<ScreenTransition>().White();
    }

    void Update()
    {
        if ((Input.anyKeyDown || Input.anyKey) && !HasStarted)
        {
            HasStarted = true;
            StartedEvent.Invoke();
            Timer.Instance.StartTimer();
        }

        if (Input.GetButtonDown("Cancel"))
        {
            if (!InGameCanvasManager.Instance.OnMenu)
                InGameCanvasManager.Instance.OpenMenu();
            else
                InGameCanvasManager.Instance.CloseMenu();
        }

        if (Input.GetKeyDown(KeyCode.F12))
        {
            var date = System.DateTime.Now.ToString();
            date = date.Replace("/", "-");
            date = date.Replace(" ", "_");
            date = date.Replace(":", "-");

            ScreenCapture.CaptureScreenshot(Application.dataPath + date + ".png");
        }
    }
}
