﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnScreenInput : MonoBehaviour
{
    private PlayerController _player;

    private bool _holdingJump = false;

    private bool _holdingRigth = false;
    private bool _holdingLeft = false;

    public void HoldingRight()
    {
        _holdingRigth = true;
    }
    public void ReleaseRight()
    {
        _holdingRigth = false;
    }

    public void HoldingLeft()
    {
        _holdingLeft = true;
    }
    public void ReleaseLeft()
    {
        _holdingLeft = false;
    }

    public void HoldingJump()
    {
        _holdingJump = true;

        if (_player.CanJump)
        {
            if (_player.Grounded || _player.Riding || _player.OnWater)
                _player.PressedJump = true;
        }
    }

    public void ReleaseJump()
    {
        _holdingJump = false;
    }

    public void PressedDown()
    {
        if (_player.CanDrop)
            _player.PressedDown = true;
    }

    void Start()
    {
        _player = Player.Instance.Controller;

        // Checa se está usando esse tipo de controle
        if (PlayerSettingsSave.Load().SwipeControlls)
            gameObject.SetActive(false);
        else
            _player.Type = PlayerController.ControllerType.OnScreen;
    }

    void Update()
    {
        // Passa as informações para o Controller

        // Movement
        if (!_holdingLeft && !_holdingRigth)
            _player.xInput = 0;

        if (_holdingRigth)
        {
            _player.xInput = 1;
        }
        else if (_holdingLeft)
        {
            _player.xInput = -1;
        }

        // Jump
        _player._holdingJump = _holdingJump;
    }
}
