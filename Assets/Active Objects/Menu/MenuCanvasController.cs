﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuCanvasController : MonoBehaviour
{
    public Transform Target;
    public float Speed = 5;

    [Header("Regions")]
    public Transform NewTarget;
    public Transform Screen;
    public Transform Out;

    private bool _moving = false;

    public void Move(Transform newTarget)
    {
        if(!_moving)
            StartCoroutine(CMove(newTarget));
    }

    public IEnumerator CMove(Transform t)
    {
        _moving = true;
        float i = 0;

        var tPos = t.position;
        var targetPos = Target.position;

        while(i < 1)
        {
            var newTpos = Vector3.Lerp(NewTarget.position, Screen.position, i);
            newTpos.y = tPos.y;
            t.position = newTpos;

            var newTargetPos = Vector3.Lerp(Screen.position, Out.position, i);
            newTargetPos.y = targetPos.y;
            Target.position = newTargetPos;

            i += Time.deltaTime * Speed;
            yield return null;
        }

        t.position = Screen.position;
        Target.position = Out.position;

        Target = t;
        _moving = false;
        yield return null;
    }

    void Start()
    {

    }

    void Update()
    {

    }
}
