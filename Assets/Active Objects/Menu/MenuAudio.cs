﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using System.IO;

public class MenuAudio : MonoBehaviour
{
    private bool _muteSFX = false;
    private bool _muteOST = false;

    public Sprite[] State;

    public Button SFX;
    public Button Music;

    private AudioMixer _mixer;

    private void Awake()
    {
        _mixer = Resources.Load<AudioMixer>("MainAudioMixer");
    }

    void Start()
    {
        // Checa Saves
        var settings = PlayerSettingsSave.Load();
        if (settings != null)
        {
            if (settings.MuteSFX)
            {
                _muteSFX = true;
                _mixer.SetFloat("SFXVolume", -80);
                SFX.GetComponent<Image>().sprite = State[1];
            }
            else
                _mixer.SetFloat("SFXVolume", 0);

            if (settings.MuteOST)
            {
                _muteOST = true;
                _mixer.SetFloat("MusicVolume", -80);
                Music.GetComponent<Image>().sprite = State[1];
            }
            else
                _mixer.SetFloat("MusicVolume", 0);
        }
        else
        {
            _mixer.SetFloat("MusicVolume", 0);
            _mixer.SetFloat("SFXVolume", 0);
        }
    }

    public void SFXSwitch()
    {
        _muteSFX = !_muteSFX;

        if (_muteSFX)
        {
            SFX.GetComponent<Image>().sprite = State[1];
            _mixer.SetFloat("SFXVolume", -80);
        }
        else
        {
            SFX.GetComponent<Image>().sprite = State[0];
            PlayerPrefs.SetFloat("mute_sfx", 0);
            _mixer.SetFloat("SFXVolume", 0);
        }

        Save();
    }

    public void MusicSwitch()
    {
        _muteOST = !_muteOST;
        if (_muteOST)
        {
            Music.GetComponent<Image>().sprite = State[1];
            PlayerPrefs.SetFloat("mute_ost", 1);
            _mixer.SetFloat("MusicVolume", -80);
        }
        else
        {
            Music.GetComponent<Image>().sprite = State[0];
            PlayerPrefs.SetFloat("mute_ost", 0);
            _mixer.SetFloat("MusicVolume", 0);
        }

        Save();
    }

    public void Save()
    {
        PlayerSettingsSave settings = new PlayerSettingsSave(_muteSFX, _muteOST);
        PlayerSettingsSave.Save(settings);
    }

    void Update()
    {

    }
}
