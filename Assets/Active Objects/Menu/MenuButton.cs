﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuButton : MonoBehaviour
{
    private Button _button;

    public Text Time;


    public Color Color
    {
        set
        {
            var c = _button.colors;

            c.normalColor = value;
            _button.colors = c;
        }
    }

    public void SetTime(float time)
    {
        int seconds = (int)time % 60;
        float fractions = time * 100;
        fractions = (fractions % 100);

        string text = string.Format("{0}:{1:00}", seconds, fractions);

        Time.text = text;
    }

    private void Awake()
    {
        _button = GetComponent<Button>();
    }

    void Start()
    {

    }

    void Update()
    {

    }
}
