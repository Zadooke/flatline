﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class PlayerSettingsSave
{
    public bool MuteSFX;
    public bool MuteOST;

    public bool SwipeControlls = false;

    public PlayerSettingsSave()
    {
        MuteOST = false;
        MuteSFX = false;
    }

    public PlayerSettingsSave(bool sfx, bool ost)
    {
        MuteSFX = sfx;
        MuteOST = ost;
    }

    public void Save()
    {
        // Cria um JSON
        var json = JsonUtility.ToJson(this);
        var path = Application.persistentDataPath + "/player.fps";

        if (File.Exists(path))
        {
            var file = new StreamWriter(path);
            file.Write(json);

            file.Close();
            file.Dispose();
        }
        else
        {
            var file = File.CreateText(path);
            file.Write(json);

            file.Close();
            file.Dispose();
        }
    }

    public static void Save(PlayerSettingsSave settings)
    {
        // Cria um JSON
        var json = JsonUtility.ToJson(settings);
        var path = Application.persistentDataPath + "/player.fps";

        if (File.Exists(path))
        {
            var file = new StreamWriter(path);
            file.Write(json);

            file.Close();
            file.Dispose();
        }
        else
        {
            var file = File.CreateText(path);
            file.Write(json);

            file.Close();
            file.Dispose();
        }
    }

    public static PlayerSettingsSave Load()
    {
        var path = Application.persistentDataPath + "/player.fps";

        if (File.Exists(path))
        {
            string file = File.ReadAllText(path);
            PlayerSettingsSave json = new PlayerSettingsSave();

            JsonUtility.FromJsonOverwrite(file, json);
            return json;
        }
        else
        {
            return null;
        }
    }
}
