﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelect : MonoBehaviour
{
    public LevelData Levels;

    public MenuButton[] Buttons;

    public Color GoldSplit;
    public Color SilverSplit;
    public Color BronzeSplit;

    void Start()
    {
        if (Levels.Load() == null)
        {
            Levels.Clear();
            Levels.Save();
        }

        for (int i = 0; i < Levels.Levels.Length; i++)
        {
            var current = Levels.Levels[i];

            Buttons[i].gameObject.SetActive(current.Unlocked);
            Buttons[i].SetTime(current.Time);

            if (current.Time == 999)
            {
                Buttons[i].SetTime(0.0f);
                continue;
            }

            Buttons[i].Color = BronzeSplit;

            if (current.Time < current.SilverSplit)
                Buttons[i].Color = SilverSplit;

            if (current.Time < current.GoldSplit)
                Buttons[i].Color = GoldSplit;

        }
    }

    void Update()
    {

    }
}
