﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateWhenCompleted : MonoBehaviour
{
    public LevelData World;

    void Start()
    {
        gameObject.SetActive(World.Completed);
    }
}
