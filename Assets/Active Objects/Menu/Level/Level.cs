﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level : MonoBehaviour
{
    private LevelData _levelData;

    public int ID;
    public float Time;

    public int WorldID = 0;

    public string NextLevel;

    [Header("Audio")]
    [SerializeField]
    private GameObject _audioManager;
    public AudioClip Track;
    public float BPM = 140;

    [Header("Camera")]
    public Vector2 CameraOffset;

    [Header("Sky")]
    public bool UseCustomColor = false;

    public Color BottomColor = new Color(0, 0, 0, 1);
    public Color TopColor = new Color(0, 0, 0, 1);


    private static Level _instance;
    public static Level Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<Level>();

            return _instance;
        }
    }

    public string LevelName
    {
        get
        {
            return SceneManager.GetActiveScene().name;
        }
    }

    public LevelData World
    {
        get { return _levelData; }
    }

    public void CompleteWorld()
    {
        _levelData.Completed = true;
        _levelData.Save();
    }

    public void Submit()
    {
        foreach (var l in _levelData.Levels)
        {
            if (l.ID == ID)
            {
                l.Time = Time;
                _levelData.Save();

                return;
            }
        }
    }

    public float GoldSplit
    {
        get
        {
            var level = GetLevel(ID);
            return level.GoldSplit;
        }
    }

    public float SilverSplit
    {
        get
        {
            var level = GetLevel(ID);
            return level.SilverSplit;
        }
    }

    public LevelHandler GetLevel(int id)
    {
        if (_levelData == null)
            return null;

        foreach (var l in _levelData.Levels)
        {
            if (l.ID == id)
                return l;
        }

        return null;
    }

    public float GetBestTime()
    {
        foreach (var l in _levelData.Levels)
        {
            if (l.ID == ID)
                return l.Time;
        }

        return 0;
    }

    public void Unlock(int ID)
    {
        foreach (var l in _levelData.Levels)
        {
            if (l.ID == ID)
            {
                l.Unlocked = true;
                _levelData.Save();

                return;
            }
        }
    }

    public void UpdateTime(float time)
    {
        if (time < GetBestTime())
        {
            Time = time;
            Submit();
        }
    }

    /// <summary>
    /// Reinicia a fase atual
    /// </summary>
    public void Restart()
    {
        UnityEngine.Time.timeScale = 1.0f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void LoadNextLevel()
    {
        UnityEngine.Time.timeScale = 1.0f;
        SceneManager.LoadScene(NextLevel);
    }

    /// <summary>
    /// Vai para o Menu
    /// </summary>
    public void Menu()
    {
        UnityEngine.Time.timeScale = 1.0f;
        SceneManager.LoadScene("Menu");
    }

    private void Start()
    {
        if (FindObjectOfType<AudioManager>() == null)
        {
            Instantiate(_audioManager);
        }

        // Checa se a musica tocando é igual a musica da fase
        if (AudioManager.Instance.Track != Track || AudioManager.Instance.Track == null)
        {
            AudioManager.Instance.Play(Track);
        }

        if (UseCustomColor)
        {
            Sky.Instance.SetColors(TopColor, BottomColor);
        }
    }

    private void Awake()
    {
        _levelData = Resources.Load<LevelData>("World" + WorldID);

        _levelData.Load();

        _levelData.GetLevel(ID).Unlocked = true;
        _levelData.Save();
    }
}
