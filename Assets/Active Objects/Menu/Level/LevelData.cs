﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

[System.Serializable]
public class LevelHandler
{
    public int ID;

    public bool Unlocked;
    public float Time;
    public float SilverSplit;
    public float GoldSplit;
}


[CreateAssetMenu(fileName = "LevelData", menuName = "Level/LevelData")]
public class LevelData : ScriptableObject
{
    public LevelHandler[] Levels;

    public bool Completed = false;

    public LevelHandler GetLevel(int id)
    {
        foreach(var l in Levels)
        {
            if (l.ID == id)
                return l;
        }

        return null;
    }

    /// <summary>
    /// Reseta o Mundo
    /// </summary>
    public void Clear()
    {
        for (int i = 0; i < Levels.Length; i++)
        {
            var current = Levels[i];

            current.Time = 999;
            current.Unlocked = true;

            if (i > 0)
            {
                current.Unlocked = false;
                current.Time = 999;
            }
        }

        Completed = false;
    }

    public void Save()
    {
        // Cria um JSON
        var json = JsonUtility.ToJson(this);
        var path = Application.persistentDataPath + "/saves/" + name + ".trs";

        if (!Directory.Exists(Application.persistentDataPath + "/saves"))
            Directory.CreateDirectory(Application.persistentDataPath + "/saves");

        if (File.Exists(path))
        {
            var file = new StreamWriter(path);
            file.Write(json);

            file.Close();
            file.Dispose();
        }
        else
        {
            var file = File.CreateText(path);
            file.Write(json);

            file.Close();
            file.Dispose();
        }
    }

    public LevelData Load()
    {
        string path = Application.persistentDataPath + "/saves/" + name + ".trs";

        if (File.Exists(path))
        {
            string file = File.ReadAllText(path);
            LevelData json = new LevelData();

            JsonUtility.FromJsonOverwrite(file, json);

            if (json != null)
            {
                Levels = json.Levels;
            }

            return this;
        }
        else
        {
            return null;
        }
    }
}
