﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    [Header("Settings")]
    public Button ControlType;

    public void PlayButton(string scene)
    {
        SceneManager.LoadScene(scene);
    }

    public void ChangeControlType()
    {
        // Pega o Control Type atual
        var current = PlayerSettingsSave.Load();

        // Inverte
        current.SwipeControlls = !current.SwipeControlls;

        // Salva
        current.Save();

        // Atualiza o button
        if (current.SwipeControlls)
            ControlType.GetComponentInChildren<Text>().text = "Swipe";
        else
            ControlType.GetComponentInChildren<Text>().text = "Classic";
    }

    void Start()
    {
        if (AudioManager.Instance != null)
            Destroy(AudioManager.Instance.gameObject);

        // Pega o Control Type atual
        var current = PlayerSettingsSave.Load();

        if(current == null) // Cria um novo
        {
            PlayerSettingsSave save = new PlayerSettingsSave();
            save.MuteOST = false;
            save.MuteSFX = false;
            save.SwipeControlls = false;

            save.Save();

            ControlType.GetComponentInChildren<Text>().text = "Classic";

            // Reseta Fases
            Resources.Load<LevelData>("World0").Clear();
        }
        else
        {
            // Atualiza o button
            if (current.SwipeControlls)
                ControlType.GetComponentInChildren<Text>().text = "Swipe";
            else
                ControlType.GetComponentInChildren<Text>().text = "Classic";
        }


    }

    void Update()
    {

    }
}
