﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    private Text _text;

    private bool _counting = false;

    private float _goldSplit;
    private float _silverSplit;

    public Color Gold;
    public Color Silver;
    public Color Bronze;

    public enum Split
    {
        Gold,
        Silver,
        Bronze
    };

    public Split CurrentSplit
    {
        get
        {
            if (_currentTime < _goldSplit)
                return Split.Gold;

            if (_currentTime < _silverSplit)
                return Split.Silver;

            return Split.Bronze;
        }
    }

    private float _currentTime = 0.0f;
    public float CurrentTime
    {
        get
        {
            return _currentTime;
        }
        set
        {
            _currentTime = value;
        }
    }

    private static Timer _instance;
    public static Timer Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<Timer>();

            return _instance;
        }
    }

    public Text Text
    {
        get
        {
            return _text;
        }

        set
        {
            _text = value;
        }
    }

    public bool Counting
    {
        get
        {
            return _counting;
        }

        set
        {
            _counting = value;
        }
    }

    public void StartTimer()
    {
        _counting = true;
    }

    public void SetText(float time)
    {
        _currentTime = time;

        int seconds = (int)_currentTime % 60;
        float frac = _currentTime * 100;
        frac = (frac % 100);

        _text.text = string.Format("{0}:{1:00}", seconds, frac);
    }

    /// <summary>
    /// Retorna uma string formatada com o tempo, no modelo segundos:milissegundos
    /// </summary>
    /// <param name="time"></param>
    /// <returns></returns>
    public static string FloatToTime(float time)
    {
        int seconds = (int)time % 60;
        float frac = time * 100;
        frac = (frac % 100);

        var s = string.Format("{0}:{1:00}", seconds, frac);
        return s;
    }

    private void Awake()
    {
        _text = GetComponent<Text>();
    }

    void Start()
    {
        // Pega o tempo dos Splits

        if(Level.Instance == null)
        {
            gameObject.SetActive(false);
            return;
        }

        _silverSplit = Level.Instance.SilverSplit;
        _goldSplit = Level.Instance.GoldSplit;
    }

    void Update()
    {
        if (!_counting)
            return;

        _currentTime += Time.deltaTime;

        int seconds = (int)_currentTime % 60;
        float frac = _currentTime * 100;
        frac = (frac % 100);

        // Checa se está dentro de um gold, silver ou bronze split
        _text.color = Gold;

        if(CurrentSplit == Split.Silver)
            _text.color = Silver;

        if(CurrentSplit == Split.Bronze)
            _text.color = Bronze;

        _text.text = string.Format("{0}:{1:00}", seconds, frac);
    }


}
