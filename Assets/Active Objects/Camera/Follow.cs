﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour
{
    private Rigidbody2D _body;

    public PlayerController Controller;
    public Vector2 Offset;

    public float Speed = 5.0f;
    public float SmoothTime = 1.0f;
    public float MaxSpeed = 10.0f;

    private Transform _target;
    private Vector3 _velocity;
    private Vector3 _targetPos;

    private float y;

    [Header("Offset Speeds")]

    [HideInInspector]
    public float NewOffset;

    public float VelocityChange;
    public float VelocityChangeTime = 1.0f;
    public float MaxVelocityChange;

    public bool Shaking = false;
    public bool Locked = false;

    public void Shake(float time = 0.1f, float strenght = 0.5f)
    {
        if (Shaking)
            return;

        StartCoroutine(CShake(time, strenght));
    }

    private IEnumerator CShake(float time, float strenght)
    {
        Shaking = true;
        var originalPos = transform.position;
        float t = 0;

        while (t < time)
        {
            t += Time.deltaTime;

            Vector3 newPos = originalPos + (Random.insideUnitCircle * strenght).xyz(transform.position);
            newPos.x = transform.position.x;
            transform.position = newPos;

            // Reduz a força
            strenght -= (strenght / 10);

            yield return null;
        }

        var pos = originalPos;
        originalPos.x = transform.position.x;

        transform.position = pos;
        Shaking = false;

        yield return null;
    }

    private void Awake()
    {
        _body = GetComponent<Rigidbody2D>();

        Application.targetFrameRate = 60;
    }

    void Start()
    {
        Controller = Player.Instance.GetComponent<PlayerController>();
        _target = Controller.transform;

        if (_target == null)
            throw new System.Exception("Não tem Target!");

        if (Level.Instance != null)
            Offset += Level.Instance.CameraOffset;

        y = _target.position.y + Offset.y;
    }

    void Update()
    {
        if (Shaking || _target == null)
            return;
    }

    private void FixedUpdate()
    {
        if (Shaking)
            return;
    }

    private void LateUpdate()
    {
        if (_target == null)
            return;

        if (!Locked)
            NewOffset = Controller.Velocity.x * 0.2f;

        Offset.x = Mathf.SmoothDamp(Offset.x, NewOffset, ref VelocityChange, VelocityChangeTime, MaxVelocityChange);

        _targetPos = _target.position + Offset.xyz(_target.position);

        if (!Shaking)
            _targetPos.y = Offset.y;
        else
            _targetPos.y = transform.position.y;

        _targetPos.z = -10;

        transform.position = _targetPos;
    }
}
