﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.PostProcessing;

/// <summary>
/// Cuida da musica de cada Fase, é iniciado pelo Level
/// </summary>
public class AudioManager : MonoBehaviour
{
    private AudioSource _source;

    public float MinVolume = 0.5f;

    public AudioClip Track;
    public AudioClip LoopDrum;

    public UnityEvent Beat;

    private float _bpm;
    private float _bpmTimer = 0;

    private static AudioManager _instance;
    public static AudioManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<AudioManager>();

            return _instance;
        }
    }

    public void Play(AudioClip track)
    {
        _bpm = 1 / (Level.Instance.BPM / 60);

        Track = track;
        _source.clip = Track;
        _source.Play();
    }

    private void Awake()
    {
        _source = GetComponent<AudioSource>();
    }

    void Start()
    {
        DontDestroyOnLoad(this);

        _source.clip = Track;
        _source.Play();

        _bpm = 1 / (Level.Instance.BPM / 60); // Tempo para cada Beat da musica
    }

    void Update()
    {
        _bpmTimer += Time.deltaTime;
            
        if (_bpmTimer >= _bpm)
        {
            _bpmTimer = 0.0f;
            Beat.Invoke();
        }
    }
}
