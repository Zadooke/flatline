﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;


public class SceneExplorer : EditorWindow
{
    private static List<string> _scenes;
    private static int _count;

    private Vector2 _scrollPosition;

    private static void LoadScenes()
    {
        _scenes = new List<string>();
        _count = EditorSceneManager.sceneCountInBuildSettings;


        for (int i = 0; i < _count; i++)
        {
            _scenes.Add(System.IO.Path.GetFileNameWithoutExtension(UnityEngine.SceneManagement.SceneUtility.GetScenePathByBuildIndex(i)));
        }
    }

    [MenuItem("Window/Scene Explorer")]
    public static void ShowWindow()
    {
        SceneExplorer window = EditorWindow.GetWindow<SceneExplorer>();

        // Pega todas as cenas que estão na Build
        LoadScenes();
    }

    private void OnGUI()
    {
        if (_scenes == null)
            _scenes = new List<string>();

        if (_scenes.Count <= 0)
            LoadScenes();

        if (GUILayout.Button("Reload"))
            LoadScenes();

        GUILayout.Space(10);

        _scrollPosition = EditorGUILayout.BeginScrollView(_scrollPosition);

        for (int i = 0; i < _count; i++)
        {
            if (GUILayout.Button(_scenes[i]))
            {
                var path = UnityEngine.SceneManagement.SceneUtility.GetScenePathByBuildIndex(i);
                EditorSceneManager.OpenScene(path, OpenSceneMode.Single);
            }
        }

        EditorGUILayout.EndScrollView();
    }
}
