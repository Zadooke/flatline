﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

[CustomEditor(typeof(Cable), true), CanEditMultipleObjects]
public class CableEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        Cable script = (Cable)target;

        //if (GUILayout.Button("Clear"))
        //{
        //    // Varre a lista de selecionados, procurando todos os Cables
        //    List<Cable> cables = new List<Cable>();

        //    foreach (var o in Selection.transforms)
        //    {
        //        if (o.GetComponent<Cable>())
        //            cables.Add(o.GetComponent<Cable>());
        //    }

        //    foreach (var c in cables)
        //    {
        //        foreach(var p in c.Posts)
        //        {
        //            DestroyImmediate(p.gameObject);
        //        }

        //        c.Posts = new List<GameObject>();
        //    }

        //    Debug.Log("Cleared " + cables.Count + " cables");
        //    EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());
        //}

        if (GUILayout.Button("Bake"))
        {
            // Varre a lista de selecionados, procurando todos os Cables
            List<Cable> cables = new List<Cable>();

            foreach (var o in Selection.transforms)
            {
                if (o.GetComponent<Cable>())
                    cables.Add(o.GetComponent<Cable>());
            }

            int success = 0;
            int failed = 0;

            // Percorre a lista de Cables
            foreach (var c in cables)
            {
                if (c.Bake() == -1)
                    failed++;
                else
                {
                    success++;
                }
            }

            Debug.Log("Finished Baking Posts!\nSucessfully baked " + success + " cables, with " + failed + " failures!");
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());
        }
    }
}
