﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

[CustomEditor(typeof(LevelCreator))]
public class LevelCreatorEditor : Editor
{
    public string NewLevelName = "";

    //private void OnEnable()
    //{
    //    LevelCreatorWindow window = EditorWindow.GetWindow<LevelCreatorWindow>();
    //    window.Script = (LevelCreator)target;
    //}

    private void OnGUI()
    {
        // Código da Janela
        LevelCreator script = (LevelCreator)target;

        foreach (var obj in script.Objects)
        {
            GUILayout.BeginHorizontal();

            GameObject go = obj.Object;
            go = (GameObject)EditorGUILayout.ObjectField(go, go.GetType(), GUILayout.MaxWidth(300));

            if (go != obj.Object)
                EditorUtility.SetDirty(target);

            obj.Object = go;

            bool breakPrefab = obj.ShouldBreakPrefab;
            breakPrefab = EditorGUILayout.Toggle("Should Break Prefab", breakPrefab);

            if (breakPrefab != obj.ShouldBreakPrefab) // Mudou
                EditorUtility.SetDirty(target);

            obj.ShouldBreakPrefab = breakPrefab;

            GUILayout.EndHorizontal();
        }

        GUILayout.Label("New Scene Name:");

        GUILayout.BeginHorizontal();

        NewLevelName = GUILayout.TextField(NewLevelName);

        if (NewLevelName != "")
        {
            if (GUILayout.Button("Create New Level", GUILayout.Width(200)))
            {
                var objs = script.GetLevelInfo();

                var scene = EditorSceneManager.NewScene(NewSceneSetup.EmptyScene, NewSceneMode.Single);

                foreach (var obj in objs)
                {
                    if (obj.ShouldBreakPrefab)
                    {
                        var instanced = Instantiate(obj.Object);
                        instanced.name = instanced.name.Split('(')[0];
                    }
                    else
                    {
                        var instanced = PrefabUtility.InstantiatePrefab(obj.Object);
                        instanced.name = instanced.name.Split('(')[0];
                    }
                }

                EditorSceneManager.SaveScene(scene, "Assets/Scenes/" + NewLevelName + ".unity");
            }
        }

        GUILayout.EndHorizontal();
    }
}

[CreateAssetMenu(fileName = "Level Creator", menuName = "Level/LevelCreator")]
public class LevelCreator : ScriptableObject
{
    [Header("Objects to Add to the New Scene:")]
    public LevelCreatorObject[] Objects;

    public LevelCreatorObject[] GetLevelInfo()
    {
        if (Objects.Length <= 0)
            return null;

        return Objects;
    }

}

[System.Serializable]
public class LevelCreatorObject
{
    public GameObject Object;
    public bool ShouldBreakPrefab = false;
}

public class LevelCreatorWindow : EditorWindow
{
    public LevelCreator Script;

    public string NewLevelName;

    [MenuItem("Window/LevelCreator")]
    public static void ShowWindow()
    {
        LevelCreatorWindow window = EditorWindow.GetWindow<LevelCreatorWindow>();

        window.Script = Resources.Load<LevelCreator>("Level Creator");
        window.Show();
    }

    private void OnGUI()
    {
        if (Script == null)
            return;

        // Código da Janela
        GUILayout.Label("New Scene Objects:");
        foreach (var obj in Script.Objects)
        {
            GUILayout.BeginHorizontal();

            GameObject go = obj.Object;
            go = (GameObject)EditorGUILayout.ObjectField(go, go.GetType(), GUILayout.MaxWidth(300));

            if (go != obj.Object)
                EditorUtility.SetDirty(Script);

            obj.Object = go;

            bool breakPrefab = obj.ShouldBreakPrefab;
            breakPrefab = EditorGUILayout.Toggle("Should Break Prefab", breakPrefab);

            if (breakPrefab != obj.ShouldBreakPrefab) // Mudou
                EditorUtility.SetDirty(Script);

            obj.ShouldBreakPrefab = breakPrefab;

            GUILayout.EndHorizontal();
        }

        GUILayout.Label("New Scene Name:");

        GUILayout.BeginHorizontal();

        NewLevelName = GUILayout.TextField(NewLevelName);

        if (NewLevelName != "")
        {
            if (GUILayout.Button("Create New Level", GUILayout.Width(200)))
            {
                var objs = Script.GetLevelInfo();

                var scene = EditorSceneManager.NewScene(NewSceneSetup.EmptyScene, NewSceneMode.Single);

                foreach (var obj in objs)
                {
                    if (obj.ShouldBreakPrefab)
                    {
                        var instanced = Instantiate(obj.Object);
                        instanced.name = instanced.name.Split('(')[0];
                    }
                    else
                    {
                        var instanced = PrefabUtility.InstantiatePrefab(obj.Object);
                        instanced.name = instanced.name.Split('(')[0];
                    }
                }

                EditorSceneManager.SaveScene(scene, "Assets/Scenes/" + NewLevelName + ".unity");

                //EditorBuildSettingsScene editorScene = new EditorBuildSettingsScene("Assets/Scenes/" + NewLevelName + ".unity", true);

                //var original = EditorBuildSettings.scenes;
                //var newScenes = new EditorBuildSettingsScene[original.Length + 1];
                //newScenes[newScenes.Length - 1] = editorScene;

                //EditorBuildSettings.scenes = newScenes;
            }
        }

        GUILayout.EndHorizontal();
    }
}