﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(LevelData))]
public class LevelDataEditor : Editor
{
    public void ShowExplorer(string itemPath)
    {
        itemPath = itemPath.Replace(@"/", @"\");   // explorer doesn't like front slashes
        System.Diagnostics.Process.Start("explorer.exe", "/select," + itemPath);
    }

    public override void OnInspectorGUI()
    {
        //DrawDefaultInspector();

        LevelData script = (LevelData)target;

        foreach (var level in script.Levels)
        {
            GUILayout.BeginHorizontal();

            // ID
            var lvlID = level.ID;
            GUILayout.Label("Level ID:");
            lvlID = EditorGUILayout.IntField(lvlID, GUILayout.MaxWidth(250));

            level.ID = lvlID;

            if (lvlID != level.ID)
                EditorUtility.SetDirty(target);

            // Time
            var lvlTime = level.Time;
            GUILayout.Label("Level Time:");
            lvlTime = EditorGUILayout.FloatField(lvlTime);

            level.Time = lvlTime;

            if (lvlTime != level.Time)
                EditorUtility.SetDirty(target);

            // Silver Split
            var silverTime = level.SilverSplit;
            GUILayout.Label("Silver Split:");
            silverTime = EditorGUILayout.FloatField(silverTime);

            level.SilverSplit = silverTime;
            EditorUtility.SetDirty(target);

            // Gold Split

            var goldTime = level.GoldSplit;
            GUILayout.Label("Gold Split:");
            goldTime = EditorGUILayout.FloatField(goldTime);

            level.GoldSplit = goldTime;
            EditorUtility.SetDirty(target);

            // Unlocked
            var unlocked = level.Unlocked;
            GUILayout.Label("Unlocked:");
            unlocked = EditorGUILayout.Toggle(unlocked);

            level.Unlocked = unlocked;

            if (unlocked != level.Unlocked)
                EditorUtility.SetDirty(target);

            GUILayout.EndHorizontal();
        }

        GUILayout.Space(10);

        GUILayout.BeginHorizontal();

        GUILayout.Label("Completed:");

        var completed = script.Completed;
        completed = EditorGUILayout.Toggle(completed);

        script.Completed = completed;
        if (completed != script.Completed)
            EditorUtility.SetDirty(target);

        GUILayout.EndHorizontal();

        GUILayout.Space(10);

        GUILayout.BeginHorizontal();

        if (GUILayout.Button("Save"))
            script.Save();

        if (GUILayout.Button("Load"))
            script.Load();

        GUILayout.EndHorizontal();

        if (GUILayout.Button("Reset"))
            script.Clear();

        if (GUILayout.Button("Open Folder Location"))
            ShowExplorer(Application.persistentDataPath + "/saves");
    }


}
