﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Level))]
public class LevelEditor : Editor
{
    public override void OnInspectorGUI()
    {
        Level script = (Level)target;

        DrawDefaultInspector();

        //GUILayout.Space(20);
        //if(GUILayout.Button("Open World Data"))
        //{
        //    // Abre o arquivo do mundo selecionado
        //}
    }
}
