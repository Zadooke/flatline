﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TestBuilder
{
    [MenuItem("Tools/TestBuild Android")]
    public static void BuildAndroid()
    {
        // Get filename.
        string path = EditorUtility.SaveFolderPanel("Choose Location of Built Game", "", "");

        // Pega o caminho para a cena atual
        string[] levels = new string[] { SceneManager.GetActiveScene().path };

        // Build player.
        BuildPipeline.BuildPlayer(levels, path + "/test.apk", BuildTarget.Android, BuildOptions.None);
    }

    [MenuItem("Tools/TestBuild PC")]
    public static void BuildPC()
    {
        // Get filename.
        string path = EditorUtility.SaveFolderPanel("Choose Location of Built Game", "", "");

        // Pega o caminho para a cena atual
        string[] levels = new string[] { SceneManager.GetActiveScene().path };

        // Build player.
        BuildPipeline.BuildPlayer(levels, path + "/test.exe", BuildTarget.StandaloneWindows, BuildOptions.None);

        // Run the game (Process class from System.Diagnostics).
        Process proc = new Process();
        proc.StartInfo.FileName = path + "/test.exe";
        proc.Start();
    }
}
