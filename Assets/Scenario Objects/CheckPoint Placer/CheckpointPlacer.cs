﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointPlacer : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        var player = collision.GetComponent<PlayerController>();

        if (player != null)
            player.PlaceCheckpoint(transform.position);
    }
}
