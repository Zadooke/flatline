﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedCable : Cable
{
    private BoxCollider2D _col;
    private Audio2D _audio2d;

    public LayerMask PlayerLayer;

    public int Counter = 0;
    public int CounterLimit = 3;

    // Aumenta 1 Counter por Rate
    public float Rate = 1.0f;
    private float _timer;

    private Color _startingColor;

    public Color FlashColor;
    public Color DamageColor;

    public void Restart()
    {
        _timer = 0;
        Counter = 0;
    }

    public void Count()
    {
        if (Player.Instance == null || !Player.Instance.HasStarted)
        {
            Counter = 0;
            return;
        }

        _renderer.color = FlashColor;
        Invoke("ResetColor", 0.1f);

        _audio2d.PlayAudioClip("redCable_blip");

        Counter++;

        if(Counter > CounterLimit)
        {
            // Flash Maior
            _renderer.color = DamageColor;
            _audio2d.PlayAudioClip("redCable_damage");
            Invoke("ResetColor", 0.1f);

            Counter = 0;

            // Da dano
            if (_col.IsTouchingLayers(PlayerLayer.value))
            {
                Player.Instance.Die();
            }
        }
    }

    private void ResetColor()
    {
        _renderer.color = _startingColor;
    }

    private void Awake()
    {
        base.Awake();

        _col = GetComponent<BoxCollider2D>();
        _audio2d = GetComponent<Audio2D>();

        _startingColor = _renderer.color;
    }

    void Start()
    {
        base.Start();

        Player.Instance.StartedEvent.AddListener(Restart);
        AudioManager.Instance.Beat.AddListener(Count);
    }

    void Update()
    {
        base.Update();

        //if (Player.Instance == null)
        //    return;

        //if (Player.Instance.Started == false)
        //{
        //    _timer = 0;
        //    Counter = 0;

        //    return;
        //}

        //_timer += Time.deltaTime;

        //if (_timer >= 0.1f)
        //    _renderer.color = _startingColor;

        //if (_timer > Rate)
        //{
        //    // Flash
        //    _renderer.color = FlashColor;
        //    _audio2d.PlayAudioClip("redCable_blip");


        //    Counter++;
        //    _timer = 0;
        //}

        //if (Counter > CounterLimit)
        //{
        //    // Flash Maior
        //    _renderer.color = DamageColor;
        //    _audio2d.PlayAudioClip("redCable_damage");

        //    Counter = 0;

        //    // Da dano
        //    if (_col.IsTouchingLayers(PlayerLayer.value))
        //    {
        //        Player.Instance.Die();
        //    }

        //}

    }
}
