﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Cable : MonoBehaviour
{
    protected SpriteRenderer _renderer;

    public float MaxVelocity = 10.0f;

    public ParticleSystem Particles;

    public GameObject Post;
    public List<GameObject> Posts = new List<GameObject>();

    [Header("Events")]
    public UnityEvent OnEnter;
    public UnityEvent OnExit;

    public int Bake()
    {
        if (Post == null)
        {
            Debug.LogError("Post " + gameObject.name + " does not have a Post prefab, unable to bake");
            return -1;
        }

        // Pega a escala em X atual, transformada em int 
        int scale = (int)transform.localScale.x;

        int numPosts = 2; // Sempre começa com 2 postes, um em cada ponto

        // Numero de postes será igual a Escala / 30 - 2, por causa dos 2 iniciais
        numPosts = (scale / 30) - 1;

        // Calcula o espaçamento entre cada poste, esse espaçamento será por padrão 30
        int spacement = 30;

        // Instancia

        // Limpa a lista
        foreach (var o in Posts)
        {
            DestroyImmediate(o.gameObject);
        }

        Posts = new List<GameObject>();

        // Primeiro os das pontas

        var left = Instantiate(Post, transform.position - Vector3.right * scale / 2, Quaternion.identity);
        var right = Instantiate(Post, transform.position + Vector3.right * scale / 2, Quaternion.identity);

        left.transform.parent = transform;
        right.transform.parent = transform;

        Posts.Add(left);
        Posts.Add(right);

        // Agora, o resto
        for (int i = 1; i <= numPosts; i++)
        {
            var curPos = left.transform.position + Vector3.right * spacement * i;

            var current = Instantiate(Post, curPos, Quaternion.identity);
            current.transform.parent = transform;

            Posts.Add(current);
        }

        return 0;
    }

    protected void Awake()
    {
        _renderer = GetComponent<SpriteRenderer>();
    }

    protected void Start()
    {
        var main = Particles.main;
        main.startColor = _renderer.color;

        var shape = Particles.shape;
        shape.radius = transform.localScale.x / 2;

        var emmision = Particles.emission;
        emmision.rateOverTime = 5 * shape.radius / 6;
    }

    protected void Update()
    {
    }
}
