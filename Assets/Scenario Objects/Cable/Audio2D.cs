﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Faz o som sair do lado correto do fone de ouvido, com volume correto, dependendo da posição
/// </summary>
[RequireComponent(typeof(AudioSource))]
public class Audio2D : MonoBehaviour
{
    private AudioSource _source;
    private Transform _player;

    private float _initialVolume;
    private Vector2 _distance;

    public Vector2 Area;

    public bool Pan = true;

    public AudioClip[] Clips;

    public void PlayAudioClip(string name)
    {
        foreach (var clip in Clips)
        {
            if (clip.name == name)
            {
                _source.PlayOneShot(clip);
                return;
            }
        }
    }

    /// <summary>
    /// Para cada unidade, diminui o volume por esse valor, linearmente
    /// </summary>
    public float VolumeFalloff = 0.8f;

    private void Awake()
    {
        _source = GetComponent<AudioSource>();
    }

    void Start()
    {
        _player = Player.Instance.transform;
        _initialVolume = _source.volume;
    }

    void Update()
    {
        if (_player == null)
            return;

        // Distancia
        _distance = (_player.position - transform.position).xy();

        // Volume
        _source.volume = _initialVolume - VolumeFalloff * Mathf.Log10(Mathf.Abs(_distance.x));

        // Pan
        if (Pan)
            _source.panStereo = -_distance.normalized.x;
    }
}
