﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Connector : MonoBehaviour
{
    private PlayerController _player;
    private Follow _camera;
    private LineRenderer _line;

    private float _speed = 1.0f;

    public Connector End;
    public bool Ready = true;

    private int JumpFrame = 10;

    public Color ConnectionColor = Color.white;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!Ready)
            return;

        if (collision.gameObject == _player.gameObject)
        {
            //_player.enabled = false;

            _player.transform.position = transform.position;
            _player.GetComponent<SpriteRenderer>().color = Color.white;
            _player.Velocity = Vector2.zero;

            Camera.main.GetComponent<Follow>().Locked = true;
            End.Ready = false;

            StartCoroutine(CRide());
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject == _player.gameObject && !Ready)
            Ready = true;
    }

    private IEnumerator CRide()
    {
        float distance = Vector3.Distance(transform.position, End.transform.position);
        float t = distance / _speed; // Quantos frames até chegar no destino

        Vector3 step = (End.transform.position - transform.position).normalized * _speed;

        Camera.main.GetComponent<Follow>().NewOffset = 75 * 0.2f * step.normalized.x;

        _player.UseGravity = false;
        _player.Riding = true;

        _player.CanJump = false;
        _player.CanDrop = false;

        bool jump = false;
        bool drop = false;

        float i = 0;
        while (i < t)
        {
            _player.transform.position += step;
            i++;

            if (i > (t - JumpFrame))
            {
                _player.CanJump = true;
                _player.CanDrop = true;

                if (_player.PressedJump)
                {
                    jump = true;
                }

                if(_player.PressedDown)
                {
                    drop = true;
                }
            }

            yield return new WaitForEndOfFrame();
        }
        
        if(jump)
        {
            _player.AddJumpVelocity();
            _player.Velocity.x = 75 * step.normalized.x;

            _player.PressedJump = false;

            _player.UseGravity = true;
            _player.Riding = false;

            _camera.Locked = false;
            yield break;
        }

        if (drop)
        {
            _player.AddJumpVelocity(-1);
            _player.Velocity.x = 75 * step.normalized.x;

            _player.PressedDown = false;

            _player.UseGravity = true;
            _player.Riding = false;

            _camera.Locked = false;
        }

        _camera.Locked = false;

        _player.Riding = false;
        _player.UseGravity = true;

        yield return null;
    }

    public void UpdateConnection()
    {
        if (End == null)
            return;

        _line.SetPositions(new Vector3[] { transform.position, End.transform.position });
    }

    public void Die()
    {
        StopAllCoroutines();

        _camera.Locked = false;

        _player.CanDrop = true;
        _player.CanJump = true;
        _player.Riding = false;
        _player.UseGravity = true;
    }

    private void Awake()
    {
        _line = GetComponent<LineRenderer>();
    }

    void Start()
    {
        _player = Player.Instance.Controller;
        _camera = Camera.main.GetComponent<Follow>();

        Player.Instance.Died.AddListener(Die);
    }

    void Update()
    {
        #if UNITY_EDITOR
        UpdateConnection();
        #endif
    }
}
