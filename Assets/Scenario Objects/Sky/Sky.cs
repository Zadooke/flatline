﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sky : MonoBehaviour
{
    private Material _mat;
    private PlayerController _player;

    private Transform _camera;

    public AnimationCurve BrightnessCurve;
    public float Speed = 1;

    [Range(0.0f, 1.0f)]
    public float Strenght = 0;

    private bool _animate = false;
    private float _time = 0;

    private static Sky _instance;
    public static Sky Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<Sky>();

            return _instance;
        }
    }

    public void Animate()
    {
        // Checa se o Player está rápido
        var speed = Mathf.Abs(_player.Velocity.x);

        Strenght = (1 - ((75 - speed) / 75)) - Random.Range(0.1f, 0.5f);

        //Strenght = Random.Range(0.5f, 1.0f);

        //Strenght = 1;

        _animate = true;
        _time = 0.0f;
    }

    public void SetColors(Color top, Color bot)
    {
        // Cria uma cópia do material
        Material mat = new Material(_mat);

        mat.SetColor("_TopColor", top);
        mat.SetColor("_BottomColor", bot);

        GetComponent<SpriteRenderer>().material = mat;
        _mat = mat;
    }

    private void Awake()
    {
        _mat = GetComponent<SpriteRenderer>().material;
    }

    void Start()
    {
        _player = Player.Instance.GetComponent<PlayerController>();
        _camera = Camera.main.transform;

        if (AudioManager.Instance != null)
            AudioManager.Instance.Beat.AddListener(Animate);
    }

    void Update()
    {
        if (_animate)
        {
            _mat.SetFloat("_Exposure", BrightnessCurve.Evaluate(_time) * Strenght);
            _time += Time.deltaTime * Speed;

            if (_time >= 1.0f)
            {
                _time = 0.0f;
                _animate = false;
            }
        }
    }
}
