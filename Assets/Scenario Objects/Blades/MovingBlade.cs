﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingBlade : Blade
{
    public float Speed = 5;

    [Range(0.0f, -1.0f)]
    public float Offset = 0.0f;

    private float _time;
    private float _velocity;
    
    public Vector3 Position1;
    public Vector3 Position2;

    private Vector3 _target;
    private Vector3 _starting;

    private bool _started = false;

    public void StartMoving()
    {
        _started = true;
    }

    public void ChangeTarget()
    {
        if (_target == Position1)
        {
            _starting = Position1;
            _target = Position2;
        }
        else
        {
            _starting = Position2;
            _target = Position1;
        }

        _time = 0;
    }

    void Start()
    {
        Position1 = transform.position + Position1;
        Position2 = transform.position + Position2;

        transform.position = Position1;

        _starting = Position1;
        _target = Position2;

        _velocity = Vector3.Distance(Position1, Position2) / Speed;

        Player.Instance.StartedEvent.AddListener(StartMoving);

        _time = Offset;
    }

    void Update()
    {
        if (!_started)
            return;

        _velocity = Vector3.Distance(Position1, Position2) / Speed;

        if (_time < 1)
        {
            _time += Speed * Time.deltaTime;
            transform.position = Vector3.Lerp(_starting, _target, _time);
        }
        else
            ChangeTarget();

    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.magenta;

        DebugExtension.DrawPoint(Position1 + transform.position);
        DebugExtension.DrawPoint(Position2 + transform.position);
    }
}
