﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transformer : MonoBehaviour
{
    public ParticleSystem Particles;

    public void Hit()
    {
        Particles.Emit(30);
    }

    private void OnTriggerEnter2D (Collider2D col)
    {
        var player = col.GetComponent<PlayerController>();
        
        if(player != null)
        {
            player.AddJumpVelocity(1.5f);
            player.Animator.SetTrigger("Jump");
            Hit();
        }
    }

    void Start()
    {

    }

    void Update()
    {

    }
}
