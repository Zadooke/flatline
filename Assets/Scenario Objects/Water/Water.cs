﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Water : MonoBehaviour
{
    public GameObject Particles;
    private PlayerController _player;

    private Material _mat;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == _player.gameObject)
        {
            Instantiate(Particles, collision.transform.position - Vector3.forward * 2, Quaternion.identity);
            _player.EnterWater();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject == _player.gameObject)
        {
            Instantiate(Particles, collision.transform.position - Vector3.forward * 2, Quaternion.identity);
            _player.ExitWater();
        }
    }

    private void Awake()
    {
        // Faz uma cópia do material
        _mat = new Material(GetComponent<MeshRenderer>().material);

        // Altera o valor da magnitude de acordo com a escala
        _mat.SetFloat("_Magnitude", 0.1f - (transform.localScale.x / 100));

        // Retorna o novo Material
        GetComponent<MeshRenderer>().material = _mat;
    }

    void Start()
    {
        _player = Player.Instance.GetComponent<PlayerController>();
    }

    void Update()
    {

    }
}
