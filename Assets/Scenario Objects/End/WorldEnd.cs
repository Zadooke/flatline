﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldEnd : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Level.Instance.CompleteWorld();
    }
}
