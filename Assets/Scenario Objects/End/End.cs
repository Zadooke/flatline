﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class End : MonoBehaviour
{
    public ParticleSystem Circle;
    public ParticleSystem Middle;

    private Vector3 _player;

    public void EndLevel()
    {
        Timer.Instance.Counting = false;
        Time.timeScale = 0.3f;

        Player.Instance.Explode();
        GetComponent<Audio2D>().PlayAudioClip("End");

        Player.Instance.Active = false;
        StartCoroutine(CLevelEnd());
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Player>())
        {
            EndLevel();
        }
    }

    IEnumerator CLevelEnd()
    {
        yield return new WaitForSecondsRealtime(1.0f);
        Time.timeScale = 1.0f;

        InGameCanvasManager.Instance.EndLevel();

        yield return null;
    }
}
