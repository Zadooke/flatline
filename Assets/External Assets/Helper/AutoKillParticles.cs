﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoKillParticles : MonoBehaviour
{

    void Start()
    {
        Destroy(this.gameObject, GetComponent<ParticleSystem>().main.duration);
    }

    void Update()
    {

    }
}
