﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D), typeof(Collider2D))]
public class PlatformerBody : MonoBehaviour
{
    protected Rigidbody2D _body;
    protected Collider2D _col;

    [Header("Essentials")]
    public bool UseGravity = true;

    public float Gravity = -50.0f;
    public float HighGravityMultiplyer = 1.6f;

    protected float _highGravity = -80.0f;

    /// <summary>
    /// Verifica se deve chamar a função HandleHorizontalMovement
    /// </summary>
    public bool CheckHorizontalMovement = true;

    /// <summary>
    /// Verifica se deve chamar a função HandleJump
    /// </summary>
    public bool CheckJump = true;

    public Vector2 Velocity = Vector2.zero;

    [Header("Jump")]
    public float JumpVelocity = 20.0f;
    public float CheckGroundDistance = 1.0f;
    public float MaxFallSpeed = 15;

    public LayerMask Ground;

    protected bool _jumping = false;
    public bool PressedJump = false;
    protected int _groundLayer;

    [Header("Movement")]
    [HideInInspector] public float xInput = 0.0f;
    [HideInInspector] public float yInput = 0.0f;

    [HideInInspector] public float LastXInput = 0.0f;

    public float AirAcceleration = 25.0f;
    public float GroundAcceleration = 50.0f;

    public float MaxHorizontalSpeed = 15.0f;
    protected float HorizontalMomentum = 0;

    public Vector2 Direction = Vector2.right;

    /// <summary>
    /// Valor até 1 de fricção do chão, 1.0f é muito liso, 0 é extremamente aspero.
    /// </summary>
    [Range(0.0f, 1.0f)]
    public float FloorFriction = 1.0f;

    /// <summary>
    /// Body is touching the Ground Layer
    /// </summary>
    public bool Grounded = false;
    protected bool _checkGround = true;

    protected virtual bool CheckGround(Vector3 position)
    {
        var offset = Vector2.left * (_col.bounds.extents.x - 0.1f);
        float dist = 0.5f;

        Debug.DrawRay(position.xy() + offset, Vector2.down * CheckGroundDistance, Color.red);
        Debug.DrawRay(position.xy() - offset, Vector2.down * CheckGroundDistance, Color.red);

        var groundLeft = Physics2D.Raycast(position.xy() + offset, Vector2.down, dist, _groundLayer);
        var groundRight = Physics2D.Raycast(position.xy() - offset, Vector2.down, dist, _groundLayer);

        var ground = Physics2D.Raycast(position.xy() + offset, Vector2.down, CheckGroundDistance, _groundLayer)
            | Physics2D.Raycast(position.xy() - offset, Vector2.down, CheckGroundDistance, _groundLayer);

        if (ground && _col.IsTouchingLayers(_groundLayer))
        {
            return true;
        }
        return false;
    }

    protected bool CheckSlant(Vector3 position)
    {
        var offset = Vector2.left * (_col.bounds.extents.x - 0.1f);
        float dist = 0.5f;

        Debug.DrawRay(position.xy() + offset, Vector2.down * CheckGroundDistance, Color.red);
        Debug.DrawRay(position.xy() - offset, Vector2.down * CheckGroundDistance, Color.red);

        var groundLeft = Physics2D.Raycast(position.xy() + offset, Vector2.down, dist, _groundLayer);
        var groundRight = Physics2D.Raycast(position.xy() - offset, Vector2.down, dist, _groundLayer);

        if (groundLeft.point.y != groundRight.point.y && Grounded)
            return true;

        return false;
    }

    protected virtual bool CheckWalls()
    {
        // Cria mascara de layer
        int wallLayerMask = (1 << LayerMask.NameToLayer("Ground")) | 1 << LayerMask.NameToLayer("Bullet");

        // Primeiro, checa teto
        Debug.DrawRay(transform.position, Vector2.up * _col.bounds.extents.y, Color.yellow);
        var ceiling = Physics2D.Raycast(transform.position, Vector2.up * _col.bounds.extents.y, 1.0f, wallLayerMask);
        if (ceiling && _col.IsTouchingLayers(wallLayerMask))
            Velocity.y = 0.0f;

        float dist = 0.5f;
        Vector2 dir = (Vector2.right * Velocity.x).normalized;

        Debug.DrawRay(transform.position, dir, Color.magenta);

        var wall = Physics2D.Raycast(transform.position.xy(), dir, dist, wallLayerMask);
        if (wall)
        {
            return true;
        }

        return false;
    }

    protected virtual void HandleHorizontalMovement()
    {
        // Nada apertado, ou apertou outro botão, para completamente se estiver em terra
        if ((xInput == 0 || xInput != LastXInput) && Grounded)
        {
            float decreaseSpeed = -Velocity.x * FloorFriction * 20;
            Velocity.x = Mathf.SmoothDamp(Velocity.x, 0, ref decreaseSpeed, 1.0f, MaxHorizontalSpeed, Time.fixedDeltaTime);
        }

        // Checa o ultimo input, para questão de trocar de direção
        if (Grounded)
            LastXInput = xInput;

        // Adiciona Momentum
        if (Grounded)
        {
            HorizontalMomentum = xInput * GroundAcceleration;
            var newSpeed = Velocity.x + HorizontalMomentum * Time.fixedDeltaTime;

            if (xInput > 0) // Direita
            {
                Velocity.x = (newSpeed < MaxHorizontalSpeed) ? newSpeed : MaxHorizontalSpeed;
                //Velocity.x = Mathf.Min(newSpeed, MaxHorizontalSpeed);
            }
            else if (xInput < 0) // Esquerda
            {
                Velocity.x = (newSpeed > -MaxHorizontalSpeed) ? newSpeed : -MaxHorizontalSpeed;
                //Velocity.x = Mathf.Max(newSpeed, -MaxHorizontalSpeed);
            }
        }
        else
        {
            HorizontalMomentum = xInput * AirAcceleration;
            var newSpeed = Velocity.x + HorizontalMomentum * Time.fixedDeltaTime;

            if (xInput > 0) // Direita
            {
                Velocity.x = Mathf.Min(newSpeed, MaxHorizontalSpeed);
            }
            else if (xInput < 0) // Esquerda
            {
                Velocity.x = Mathf.Max(newSpeed, -MaxHorizontalSpeed);
            }

            if (xInput == 0 || xInput != LastXInput)
            {
                if (Velocity.x > MaxHorizontalSpeed || Velocity.x < -MaxHorizontalSpeed)
                {
                    float decreaseSpeed = -Velocity.x * FloorFriction * 10;
                    Velocity.x = Mathf.SmoothDamp(Velocity.x, 0, ref decreaseSpeed, 1.0f, MaxHorizontalSpeed * 2, Time.fixedDeltaTime);

                }
            }
        }

        if (CheckWalls())
        {
            Velocity.x = 0.0f;
        }
    }

    protected virtual void HandleJump()
    {
        if (PressedJump && Grounded)
        {
            PressedJump = false;

            Velocity.y = JumpVelocity;

            _jumping = true;

            Grounded = false;
            _checkGround = false;
        }

        // Checa se precisa checar ground
        if (Velocity.y < 0)
        {
            _checkGround = true;
        }

        // Faz a checagem
        if (_checkGround)
        {
            if (CheckGround(transform.position))    // Encontra o Ground no frame atual
            {
                Grounded = true;
                _jumping = false;

                Velocity.y = 0;
                _checkGround = false;
            }
            else
            {
                Grounded = false;
                _checkGround = true;
            }
        }

    }

    //public void Jump()
    //{
    //    if (!PressedJump)
    //        PressedJump = true;
    //}

    protected virtual void Move()
    {
        /* ---------- Move ---------- */
        HandleHorizontalMovement();


        /* ---------- Pula ---------- */
        HandleJump();

        /*-------- Gravidade --------*/
        if (_jumping)
        {
            if (Velocity.y > 0)
                Velocity.y = Mathf.Max(Velocity.y + Gravity * Time.fixedDeltaTime, -MaxFallSpeed);
            else
                Velocity.y = Mathf.Max(Velocity.y + _highGravity * Time.fixedDeltaTime, -MaxFallSpeed);
        }
        else
        {
            var accel = _highGravity * Time.fixedDeltaTime;

            if (Velocity.y + accel < -MaxFallSpeed)
                Velocity.y = -MaxFallSpeed;
            else
                Velocity.y += accel;
        }

        // Retorna os valores para a Engine de fisica da Unity

        var nextPosition = transform.position.xy() + Velocity * Time.fixedDeltaTime;
        _body.MovePosition(nextPosition);
    }

    protected void Awake()
    {
        // Pega todas as referências necessárias
        _body = GetComponent<Rigidbody2D>();
        _col = GetComponent<Collider2D>();
    }

    protected void Start()
    {
        _groundLayer = Ground.value;

        _highGravity = Gravity * HighGravityMultiplyer;
    }

    protected void Update()
    {

    }

    protected void FixedUpdate()
    {
        Move();
    }
}
