﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleBounce : MonoBehaviour
{
    protected Vector2 startPos;
    private float time = 0.0f;

    public float Speed = 5.0f;

    [Range(0.0f, 1.0f)]
    public float Range = 0.1f;

    void Start()
    {
        startPos = transform.position;
    }

    void Update()
    {
        var pos = transform.position;
        time += Time.deltaTime * Speed;

        pos.y = startPos.y + (Mathf.Sin(time) * Range);
        transform.position = pos;
    }
}
